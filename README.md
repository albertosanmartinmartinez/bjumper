# Bjumper

Alberto Sanmartin technical backend test

## Installation

- Create & activate python virtual environment

```bash
pyenv virtualenv 3.11.4 venv_asanmartin_bjumper
pyenv activate venv_asanmartin_bjumper
```

- Set environment variables

```bash
export ENVIRONMENT=local
echo $ENVIRONMENT 
```

## Usage

### Docker

- Run Docker container services

```bash
docker compose up
docker compose -f docker-compose.yml up
docker compose -f docker-compose.yml up --build --remove-orphans --force-recreate
```

- Connect to Docker container services

```bash
docker exec -it bjumper_local_django_ctnr sh
```

### Django

- Create super user

```bash
python manage.py createsuperuser
```
